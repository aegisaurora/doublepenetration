#ifndef MAIN_H
#define MAIN_H

#include "loader/loader.h"
#include <sys/SRDescent.h>

class AsiPlugin : public SRDescent {
public:
	explicit AsiPlugin();
	virtual ~AsiPlugin();

	bool addHelper( HelperInfo &helper );
	bool toggleHelper( std::string_view helper, const bool &status );
	bool statusHelper( std::string_view helper );

private:
	int		  mid = -1, cid = -1, did = -1;
	CD3DFont *font;

	std::vector<HelperInfo> helpers;

	void mainloop();
	void combo( std::string combo_name );
	void draw();
};

#endif // MAIN_H
