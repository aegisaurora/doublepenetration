#include "main.h"

#include <CGame/methods.h>
#include <d3d9/d3drender.h>

#include "AirBreak.h"

static AsiPlugin *self = nullptr;

AsiPlugin::AsiPlugin() : SRDescent() {
	// Constructor
	self = this;

	font = g_class.DirectX->d3d9_CreateFont( "Tahoma", 10, 17 );
	mid	 = g_class.events->onMainLoop += std::tuple{ this, &AsiPlugin::mainloop };
	cid	 = g_class.events->onCombo += std::tuple{ this, &AsiPlugin::combo };
	did	 = g_class.DirectX->onDraw += std::tuple{ this, &AsiPlugin::draw };


	new AirBreak( this );
}

AsiPlugin::~AsiPlugin() {
	// Destructor
	if ( mid != -1 ) g_class.events->onMainLoop -= mid;
	if ( cid != -1 ) g_class.events->onCombo -= cid;
	if ( did != -1 ) g_class.DirectX->onDraw -= did;
	if ( font ) g_class.DirectX->d3d9_ReleaseFont( font );
}

bool AsiPlugin::addHelper( HelperInfo &new_helper ) {
	for ( auto &&helper : helpers )
		if ( helper.name == new_helper.name ) return false;
	helpers.push_back( new_helper );
	g_class.events->addCombo( new_helper.combo, new_helper.name );
	return true;
}

bool AsiPlugin::toggleHelper( std::string_view helper_, const bool &status ) {
	for ( auto &&helper : helpers ) {
		if ( helper.name == helper_ ) {
			helper.status = status;
			helper.statusChanged( status );
			return true;
		}
	}
	return false;
}

bool AsiPlugin::statusHelper( std::string_view helper_ ) {
	for ( auto &&helper : helpers )
		if ( helper.name == helper_ ) return helper.status;
	return false;
}

void AsiPlugin::mainloop() {
	for ( auto &&helper : helpers ) helper.mainloop();
}

void AsiPlugin::combo( std::string combo_name ) {
	for ( auto &&helper : helpers ) {
		if ( combo_name == helper.name ) {
			helper.status ^= true;
			helper.statusChanged( helper.status );
			break;
		}
	}
}

void AsiPlugin::draw() {
	std::string helpStatus;
	int			maxStringLength = 0;
	for ( auto &&helper : helpers ) {
		if ( !helper.status ) continue;
		auto helper_text = helper.render();
		auto length		 = font->DrawLength( helper_text );
		if ( length > maxStringLength ) maxStringLength = length;
		helpStatus += helper_text;
		helpStatus += "\x01";
	}
	if ( helpStatus.empty() ) return;
	if ( helpStatus.back() == '\x01' ) helpStatus.pop_back();

	auto getPos = [this]( std::string_view str, int lines = 1 ) {
		int x = ( g_class.params->BackBufferWidth - font->DrawLength( str ) ) - 5;
		int y = ( g_class.params->BackBufferHeight / 2 ) + ( font->DrawHeight() * lines );
		return std::make_tuple( x, y );
	};

	auto helpLines = SRString( helpStatus ).split( '\x01' );
	int	 line	   = 0;
	for ( auto &&helpLine : helpLines ) {
		auto [x, y] = getPos( helpLine, helpLines.size() - line++ );
		font->PrintShadow( x, y, eCdRed, helpLine );
	}
}

extern "C" __declspec( dllexport ) bool addHelper( HelperInfo &helper ) {
	if ( !self ) return false;
	return self->addHelper( helper );
}
extern "C" __declspec( dllexport ) bool toggleHelper( std::string_view helper, const bool &status ) {
	if ( !self ) return false;
	return self->toggleHelper( helper, status );
}
extern "C" __declspec( dllexport ) bool statusHelper( std::string_view helper ) {
	if ( !self ) return false;
	return self->statusHelper( helper );
}
