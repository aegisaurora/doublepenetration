#ifndef AIRBREAK_H
#define AIRBREAK_H

#include <sys/SRDescent.h>
#include "loader/loader.h"

class AirBreak : public SRDescent {
	bool		   status = false;
	HelperInfo	 helper;

public:
	AirBreak( SRDescent *parent );
	virtual ~AirBreak();
};

#endif // AIRBREAK_H
