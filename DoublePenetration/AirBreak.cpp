#include "AirBreak.h"
#include <gtasa.hpp>

extern "C" bool addHelper( HelperInfo &helper );
extern "C" bool statusHelper( std::string_view helper );
extern "C" bool toggleHelper( std::string_view helper, const bool &status );

AirBreak::AirBreak( SRDescent *parent )
	: SRDescent( parent ), helper( &status, { VK_LMENU, 0x32 }, "AirBreak" ) {
	helper.statusChanged = []( bool status ) {
		CPhysical *phys = LOCAL_PLAYER;
		if ( LOCAL_PLAYER->isDriving() ) phys = LOCAL_PLAYER->vehicle;
		if ( !status ) {
			phys->unlock();
			phys->soft			 = false;
			phys->bUsesCollision = true;
			CPad::get()->bDisablePlayerJump = false;
		}
	};
	helper.mainloop = [this] {
		if ( !status ) return;
		if ( !LOCAL_PLAYER ) return;
		if ( !LOCAL_PLAYER->isValid() ) return;
		if ( LOCAL_PLAYER->isActorDead() ) return;
		CPhysical *phys = LOCAL_PLAYER;
		if ( LOCAL_PLAYER->isDriving() ) phys = LOCAL_PLAYER->vehicle;

		if ( !phys->isLocked() ) {
			phys->lock();
			phys->soft			 = true;
			phys->bUsesCollision = false;
			CPad::get()->bDisablePlayerJump = true;
		}
		phys->matrix->right = -CCamera::TheCamera()->m_cameraMatrix.right;
		phys->matrix->up	= CCamera::TheCamera()->m_cameraMatrix.up;
		phys->matrix->at	= CCamera::TheCamera()->m_cameraMatrix.at;
		float adjust = 3;
		if ( !LOCAL_PLAYER->isDriving() ) {
			float camAngle				   = *(float *)0xB6F258 + float( 3.14159 ) / 2.0f;
			LOCAL_PLAYER->fCurrentRotation = camAngle;
			LOCAL_PLAYER->fTargetRotation  = camAngle;
			if ( g_class.events->gameKeyState( SRKeys::GameKeysOnFoot::Forward_or_Backward ) == 65408 )
				phys->matrix->pos += phys->matrix->getOffset( { 0.0f, 0.18f * adjust, 0.0f } );
			if ( g_class.events->gameKeyState( SRKeys::GameKeysOnFoot::Forward_or_Backward ) == 128 )
				phys->matrix->pos += phys->matrix->getOffset( { 0.0f, -0.18f * adjust, 0.0f } );
			if ( g_class.events->gameKeyState( SRKeys::GameKeysOnFoot::PrevWeapon_or_ZoomIn ) )
				phys->matrix->pos += phys->matrix->getOffset( { 0.0f, 0.0f, 0.12f * adjust } );
			if ( g_class.events->gameKeyState( SRKeys::GameKeysOnFoot::NextWeapon_or_ZoomOut ) )
				phys->matrix->pos += phys->matrix->getOffset( { 0.0f, 0.0f, -0.12f * adjust } );
		} else {
			if ( g_class.events->gameKeyState( SRKeys::GameKeysVehicle::Accelerate ) )
				phys->matrix->pos += phys->matrix->getOffset( { 0.0f, 0.35f * adjust, 0.0f } );
			if ( g_class.events->gameKeyState( SRKeys::GameKeysVehicle::Break_or_Reserve ) )
				phys->matrix->pos += phys->matrix->getOffset( { 0.0f, -0.35f * adjust, 0.0f } );
			if ( g_class.events->gameKeyState( SRKeys::GameKeysVehicle::Steer_Back_or_Up ) == 128 )
				phys->matrix->pos += phys->matrix->getOffset( { 0.0f, 0.0f, 0.12f * adjust } );
			if ( g_class.events->gameKeyState( SRKeys::GameKeysVehicle::Steer_Back_or_Up ) == 65408 )
				phys->matrix->pos += phys->matrix->getOffset( { 0.0f, 0.0f, -0.12f * adjust } );
		}
		if ( g_class.events->gameKeyState( SRKeys::GameKeysOnFoot::Left_or_Right ) == 65408 )
			phys->matrix->pos += phys->matrix->getOffset( { -0.18f * adjust, 0.0f, 0.0f } );
		if ( g_class.events->gameKeyState( SRKeys::GameKeysOnFoot::Left_or_Right ) == 128 )
			phys->matrix->pos += phys->matrix->getOffset( { 0.18f * adjust, 0.0f, 0.0f } );
	};
	addHelper( helper );
}

AirBreak::~AirBreak() {}
